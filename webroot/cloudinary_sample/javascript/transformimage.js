	// var cl = cloudinary.Cloudinary.new( { cloud_name: "ddwpoljax"});

		// var imageurl=cl.url("coffee");

		
		// console.log(cl.image("coffee"));

		// var tag=cl.imageTag("coffee");

		// var tr= tag.transformation().crop("fit").width(100).toHtml();

		// console.log(tr);

		
		// tr.crop("fit").width(200).height(500);
		

		// var url='http://res.cloudinary.com/ddwpoljax/image/upload/'+tr.serialize()+'/coffee';

		// loadImage(url);

		// function loadImage(imageurl){
		// 	console.log('imageurl',imageurl);

		// 	var imageEl= document.getElementById('image');
		// 	imageEl.src= imageurl;
		// }

	var app = angular.module('CloudinaryApp', []);


    app.controller('MyCtrl', ['$scope','$http',  function($scope, $http) {
       
      //  	$http({
		    //     method : "GET",
		    //     url : "../welcome.html"
		    // }).then(function mySuccess(response) {
		    // 	console.log(response.data);
		    //     $scope.myWelcome = response.data;
		    // }, function myError(response) {
		    // 	console.log(response.data);
		    //     $scope.myWelcome = response.statusText;
		    // });

        $scope.thumbnailWidth= 300;

        // $scope.imageUrl="123";
        // $scope.myimage="123";
        // $scope.public_id=apublic_id;
        // $scope.test2=$scope.test1;

        

        // $scope.public_id='coffee';
        
        // $scope.format=$scope.formatOptions[0];

        // $scope.image={
        // 	'public_id':'coffee'
        // };

        
        $scope.radius= 0;
        $scope.angle= 0;


        // $scope.originalWidth= 1280;
        // $scope.originalHeight= 848;

       
        
        $scope.cropOptions=[
        	{'name':'scale','title':'scale'},
        	{'name':'limit','title':'limit'},
        	{'name':'fill','title':'fill'},
        	{'name':'fit','title':'fit'},
        	{'name':'crop','title':'crop'},
        	{'name':'thumb','title':'thumb'},
        	{'name':'pad','title':'pad'},
        	{'name':'lfill','title':'limited fill'},
        	{'name':'lpad','title':'limit & pad'},
        	{'name':'mfit','title':'fit (scale up)'},
        	{'name':'mpad','title':'pad (no scale)'},
        ];
        $scope.crop=$scope.cropOptions[0].name;
        $scope.cropId=$scope.cropOptions[0].id;

        $scope.cropOptionsShowGravity=['crop','fill','thumb','pad'];

        $scope.gravityOptions=['none','north_west','north','north_east','west','center','east','south_west','south','south_east','xy_center','face','faces','face:center','face:auto','faces:center','faces:auto','body','body:face','ocr_test','adv_face','adv_faces','adv_eyes'];
        $scope.gravity='none';
        $scope.x=0;
        $scope.y=0;

        $scope.formatOptions=['jpg','png','gif'];
        $scope.quality=80;


        $scope.effectOptions=[
        	{'name':'art','title':'artistic filter','type':'select',
        		'select':['al_dente','athena','audrey','aurora','daguerre','eucalyptus','fes','frost','hairspray','hokusai','incognito','linen','peacock','primavera','quartz','red_rock','refresh','sizzle','sonnet','ukulele','zorro']
        	},
        	{'name':'auto_brightness','title':'auto brightness','type':'none'},
        	{'name':'auto_color','title':'auto color','type':'none'},
        	{'name':'auto_contrast','title':'auto contrast','type':'none'},
        	{'name':'auto_saturation','title':'auto saturation','type':'none'},
        	{'name':'blackwhite','title':'blackwhite','type':'none'},
        	{'name':'blue','title':'blue','type':'number','min':'-100','max':'100','default':0},
        	{'name':'blur','title':'blur','type':'number','min':1,'max':2000,'default':100},
        	{'name':'blur_faces','title':'blur_faces','type':'number','min':1,'max':2000,'default':500},
        	{'name':'blur_region','title':'blur region','type':'number','min':1,'max':2000,'default':100},
        	{'name':'brightness_hsb','title':'brightness hsb','type':'none'},
        	{'name':'cartoonify','title':'cartoonify','type':'none'},
        	{'name':'colorize','title':'colorize','type':'number','min':0,'max':100,'default':100},
        	{'name':'contrast','title':'contrast','type':'number','min':-100,'max':100,'default':0},
        	{'name':'cut_out','title':'cut out','type':'none'},
        	{'name':'displace','title':'displace','type':'none'},
        	{'name':'fill_light','title':'fill light','type':'number','min':-100,'max':100,'default':0},
        	{'name':'gamma','title':'gamma','type':'number','min':-50,'max':150,'default':0},
        	{'name':'gradient_fade','title':'gradient fade','type':'number','min':0,'max':100,'default':20},
        	{'name':'grayscale','title':'grayscale','type':'none'},
        	{'name':'green','title':'green','type':'number','min':-100,'max':100,'default':0},
        	{'name':'hue','title':'hue','type':'number','min':-100,'max':100,'default':80},
        	{'name':'improve','title':'improve','type':'none'},
        	{'name':'make_transparent','title':'make transparent','type':'number','min':0,'max':100,'default':10},
        	{'name':'mask','title':'mask','type':'none'},
        	{'name':'negate','title':'negate','type':'none'},
        	{'name':'oil_paint','title':'oil_paint','type':'number','min':0,'max':100,'default':30},
        	{'name':'ordered_dither','title':'ordered_dither','type':'number','min':0,'max':18,'default':0},
        	{'name':'pixelate','title':'pixelate','type':'number','min':1,'max':200,'default':5},
        	{'name':'pixelate_faces','title':'pixelate faces','type':'number','min':1,'max':200,'default':5},
        	{'name':'pixelate_region','title':'pixelate region','type':'number','min':1,'max':200,'default':5},
        	{'name':'red','title':'red','type':'number','min':-100,'max':100,'default':0},
        	{'name':'redeye','title':'remove redeye','type':'none'},
        	{'name':'saturation','title':'saturation','type':'number','min':-100,'max':100,'default':80},
        	{'name':'sepia','title':'sepia','type':'number','min':1,'max':100,'default':80},
        	{'name':'shadow','title':'shadow','type':'number','min':0,'max':100,'default':40},
        	{'name':'sharpen','title':'sharpen','type':'number','min':0,'max':2000,'default':100},
        	{'name':'tilt_shift','title':'tilt shift','type':'number','min':0,'max':100,'default':20},
        	{'name':'tint','title':'tint','type':'none'},
        	{'name':'trim','title':'trim','type':'number','min':0,'max':100,'default':10},
        	{'name':'unsharp_mask','title':'unsharp mask','type':'number','min':0,'max':2000,'default':100},
        	{'name':'vibrance','title':'vibrance','type':'number','min':-100,'max':100,'default':20},
        	{'name':'vignette','title':'vignette','type':'number','min':0,'max':100,'default':20},
        	
        ]

        /*
		effects skipped: 
		- too many parameters:distort, outline, sheer
		- use overlay: multiple, overlay, screen
		- require another thing: replace color
		- not on the manual: opacity_threshold
        */

        $scope.opacity=100;
        $scope.border=5;
        $scope.borderColor='#FF0000';
        $scope.zoom=0;
        $scope.aspect_ratio='';
        $scope.backgroundColor='#000000';

        $scope.applyButtonVisible=false;
        $scope.saveImageVisible=false;
        
        
        var tr = cloudinary.Transformation.new();
        var trThumbnail= cloudinary.Transformation.new();
        trThumbnail.crop('scale').width($scope.thumbnailWidth);

		// tr.crop("fit").width(500).height(200);

		// $scope.imageUrlLink='http://res.cloudinary.com/ddwpoljax/image/upload/'+$scope.public_id+'.'+$scope.format;
		// $scope.imageUrl='http://res.cloudinary.com/ddwpoljax/image/upload/'+trThumbnail.serialize()+'/'+$scope.public_id+'.'+$scope.format;

		// $scope.setImageUrl();


		// $scope.rotate= function(){
		// 	tr.angle($scope.angle);
		// 	// $scope.setImageUrl(tr);
		// }

		$scope.angleChange= function(){
			tr.angle($scope.angle);
			$scope.setApplyVisible();
			// console.log($scope.applyButtonVisible);
		}	
		$scope.radiusChange= function(){
			tr.radius($scope.radius);
			$scope.setApplyVisible();
			// console.log($scope.applyButtonVisible);
		}
		$scope.aspectRatioEnabledChange= function(){
			
			if ($scope.aspectRatioEnabled){
				$value=$scope.aspectRatioWidth+':'+$scope.aspectRatioHeight;
							

				if ($scope.widthEnabled && $scope.heightEnabled){
					$scope.heightEnabled=false;
					tr.height(null);
				}
				if (!$scope.widthEnabled && !$scope.heightEnabled){
					$scope.widthEnabled=true;
					tr.width($scope.width);
				}

				if ($scope.widthEnabled){
					$scope.height=$scope.width/$scope.aspectRatioWidth*$scope.aspectRatioHeight;
					tr.width($scope.width);				
				}else
				if ($scope.heightEnabled){
					$scope.width=$scope.height/$scope.aspectRatioHeight*$scope.aspectRatioWidth;
					tr.height($scope.height);
				}
			}else{
				$value=null;

				$scope.widthEnabled=true;
				$scope.heightEnabled=true;
				tr.width($scope.width).height($scope.height);		
			}

			tr.aspectRatio($value);		

			$scope.setApplyVisible();
		}
		
		$scope.aspectRatioWidthChange= function(){
			if ($scope.aspectRatioEnabled){
				$value=$scope.aspectRatioWidth+':'+$scope.aspectRatioHeight;
				// console.log('ar',$value);
				tr.aspectRatio($value);

				if ($scope.widthEnabled)
					tr.crop($scope.crop).width($scope.width);
				if ($scope.heightEnabled)
					tr.crop($scope.crop).height($scope.height);
			}

			$scope.setApplyVisible();
		}
		$scope.aspectRatioHeightChange= function(){
			if ($scope.aspectRatioEnabled){
				$value=$scope.aspectRatioWidth+':'+$scope.aspectRatioHeight;
				// console.log('ar',$value);
				tr.aspectRatio($value);

				if ($scope.widthEnabled)
					$scope.widthChange();
				if ($scope.heightEnabled)
					$scope.heightChange();

			}

			$scope.setApplyVisible();
		}
		$scope.widthChange= function(){
			// $scope.widthEnabled=true;
			if ($scope.widthEnabled)
				tr.crop($scope.crop).width($scope.width);

			if ($scope.aspectRatioEnabled){
				$scope.height=$scope.width/$scope.aspectRatioWidth*$scope.aspectRatioHeight;
			}

			$scope.setApplyVisible();
		}
		$scope.widthEnabledChange= function(){
			if ($scope.aspectRatioEnabled){
				if (!$scope.heightEnabled)
					$scope.widthEnabled=true;	
				else if($scope.heightEnabled)
					$scope.heightEnabled=false;
			} 

			if ($scope.widthEnabled)
				$width=$scope.width;
			else $width=null;			
			if ($scope.heightEnabled)
				$height=$scope.height;
			else $height=null;			

			tr.crop($scope.crop).width($width).height($height);

			// console.log('width enabled changed');
		

			$scope.setApplyVisible();
		}	
		$scope.heightChange= function(){
			// $scope.heightEnabled=true;
			if ($scope.heightEnabled)
				tr.crop($scope.crop).height($scope.height);

			//if aspect ratio is enabled
			if ($scope.aspectRatioEnabled){
				$scope.width=$scope.height/$scope.aspectRatioHeight*$scope.aspectRatioWidth;
			}
			$scope.setApplyVisible();
		}	
		$scope.heightEnabledChange= function(){
			if ($scope.aspectRatioEnabled){
				if (!$scope.widthEnabled)
					$scope.heightEnabled=true;	
				else if($scope.widthEnabled)
					$scope.widthEnabled=false;
			} 

			if ($scope.widthEnabled)
				$width=$scope.width;
			else $width=null;			
			if ($scope.heightEnabled)
				$height=$scope.height;
			else $height=null;			

			tr.crop($scope.crop).width($width).height($height);					

			// console.log('height enabled changed');
			

			$scope.setApplyVisible();
		}	
		$scope.cropChange= function(){
			// $scope.cropId=id;

			if ($scope.cropOptionsShowGravity.indexOf($scope.crop)==-1){
				$scope.gravity='none';
				$scope.gravityChange();				
			}
			tr.crop($scope.crop);
			$scope.setApplyVisible();
		}	
		$scope.gravityChange= function(){
			if ($scope.gravity=='none')			
				tr.gravity(null);
			else
				tr.gravity($scope.gravity);
			$scope.setApplyVisible();
		}
		$scope.xChange= function(){			
			tr.crop($scope.crop);
			tr.x($scope.x);		
			$scope.setApplyVisible();
		}
		$scope.yChange= function(){			
			tr.crop($scope.crop);
			tr.y($scope.y);		
			$scope.setApplyVisible();
		}
		$scope.formatChange= function(){			
			$scope.setApplyVisible();
		}	
		$scope.qualityChange= function(){			
			tr.quality($scope.quality);
			$scope.setApplyVisible();
		}	
		$scope.effectChange= function(){
			
			if ($scope.effect==''){
				$scope.effectData=null;
				tr.effect(null);

			}else{
			
				$scope.effectOptions.forEach(function(option){
					if (option.name==$scope.effect)
						$scope.effectData=option;
				});
				if ($scope.effectData==null){
					tr.effect(null);
				}
				else if ($scope.effectData.type=='none'){
					tr.effect($scope.effect);
				}
				else if ($scope.effectData.type=='number'){
					$scope.effectNumber=$scope.effectData.default;
					// tr.effect($scope.effect);
					$scope.effectNumberChange();
				}
				else 
				{

					tr.effect($scope.effect);
				}
			}

			$scope.setApplyVisible();
		}		
		$scope.effectNumberChange= function(){
			if ($scope.effectData!=null && $scope.effectData.type=='number'){
				tr.effect($scope.effect+':'+$scope.effectNumber);
				$scope.setApplyVisible();
			}
		}
		$scope.effectSelectChange= function(){
			if ($scope.effectData!=null && $scope.effectData.type=='select'){
				tr.effect($scope.effect+':'+$scope.effectSelect);
				$scope.setApplyVisible();
			}
		}
		$scope.opacityChange= function(){
			tr.opacity($scope.opacity);
			$scope.setApplyVisible();
		}
		$scope.borderEnabledChange= function(){
			// console.log('borderEnabledChange');
			if ($scope.borderEnabled)
				$scope.borderChange();	
			else tr.border(null);
			$scope.setApplyVisible();
		}
		$scope.borderChange= function(){
			if ($scope.borderEnabled){

				// console.log('change border');

				$borderColor=$scope.borderColor;
				$borderColor= $borderColor.substring(1);

				tr.border($scope.border+'px_solid_rgb:'+$borderColor);
				$scope.setApplyVisible();
			}
		}
		$scope.borderColorChange= function(){

			if ($scope.borderEnabled){
				$borderColor=$scope.borderColor;
				$borderColor= $borderColor.substring(1);

				tr.border($scope.border+'px_solid_rgb:'+$borderColor);
				$scope.setApplyVisible();
			}
		}
		$scope.backgroundEnabledChange= function(){
			if ($scope.backgroundEnabled)
				if ($scope.backgroundAuto)
					$scope.backgroundAutoChange();
				else
					$scope.backgroundColorChange();

			else tr.background(null);
					
			$scope.setApplyVisible();
		}
		$scope.backgroundColorChange= function(){

			if ($scope.backgroundEnabled){
				$scope.backgroundAuto=false;

				$color=$scope.backgroundColor;
				$color= $color.substring(1);
				// console.log('bg color change','rgb:'+$color);
				// tr.background('red');
				tr.background('rgb:'+$color);
				$scope.setApplyVisible();
			}
		}
		$scope.backgroundAutoChange=function(){
			// console.log('$scope.backgroundAuto',$scope.backgroundAuto);
			if ($scope.backgroundEnabled){
				if ($scope.backgroundAuto){
					tr.background('auto');
				}else{
					$color=$scope.backgroundColor;
					$color= $color.substring(1);
					
					tr.background('rgb:'+$color);
				}
				$scope.setApplyVisible();
			}
		}

		$scope.zoomChange= function(){
			tr.zoom($scope.zoom);
			$scope.setApplyVisible();
		}
	


		/**		
		
		*/

		$scope.init= function(){
			$scope.width=$scope.originalWidth;
        	$scope.height=$scope.originalHeight;

        	// $scope.aspectRatio=$scope.originalWidth+':'+$scope.originalHeight;
        	$scope.aspectRatioWidth=$scope.originalWidth;
        	$scope.aspectRatioHeight=$scope.originalHeight;
        	$scope.aspectRatioEnabled=true;
        	$scope.aspectRatioEnabledChange();

        	$scope.widthEnabled=true;
        	$scope.widthEnabledChange();
        	        	
			$scope.imageUrlLink='http://res.cloudinary.com/ddwpoljax/image/upload/'+$scope.public_id+'.'+$scope.format;
			$scope.imageUrl='http://res.cloudinary.com/ddwpoljax/image/upload/'+trThumbnail.serialize()+'/'+$scope.public_id+'.'+$scope.format;

			$scope.applyButtonVisible=false;
		}
		
		$scope.setApplyVisible= function(){

			console.log('setApplyVisible');
			
			$scope.imageUrlLink='http://res.cloudinary.com/ddwpoljax/image/upload/'+tr.serialize()+'/'+$scope.public_id+'.'+$scope.format;
			$scope.applyButtonVisible=true;

			$scope.checkError();

			// $scope.tr=tr;
		}
		$scope.checkError= function(){
			$atr=tr.toString();
			// console.log('tr',$atr);
			// console.log('$atr.indexOf(c_pad)',$atr.indexOf('c_pad'));

			// $tr1=tr
			// console.log('tr1',$tr1);
			if ($atr.indexOf('b_auto')>-1 && 
				($atr.indexOf('c_pad')==-1 && $atr.indexOf('c_lpad')==-1) && $atr.indexOf('c_mpad')==-1 )
			{
				$scope.error="Background auto can be used only with mode pad or \"limit and pad\"";
				
			}else
			$scope.error=false;
		}

		$scope.setImage= function (){

			$scope.imageUrlLink='http://res.cloudinary.com/ddwpoljax/image/upload/'+tr.serialize()+'/'+$scope.public_id+'.'+$scope.format;
			$scope.imageUrl='http://res.cloudinary.com/ddwpoljax/image/upload/'+tr.serialize()+'/'+trThumbnail.serialize()+'/'+$scope.public_id+'.'+$scope.format;

			$scope.applyButtonVisible=false;
			$scope.saveImageVisible=true;
		}

		$scope.testfunction= function(){
			$scope.test2=$scope.test1;
		}

		$scope.saveImage= function(){
			
			$scope.savingStatusVisible=true;     

			// console.log('imageUrlLink='+$scope.imageUrlLink);
			// console.log('originalImageFile='+$scope.originalImageFile);

			$http({
		        method : "POST",
		        url : "getimage.php",
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		        data: 'url='+$scope.imageUrlLink+'&filename='+$scope.originalImageFile

		    }).then(function mySuccess(response) {
		    	console.log(response.data);
		    	$scope.saveImageVisible=false;
		    	$scope.savingStatusVisible=false;
		        // $scope.myWelcome = response.data;
		    }, function myError(response) {
		    	console.log(response);
		    	$scope.savingStatusVisible=false;
		        $scope.error = response.statusText;
		    });
		}
       
    }]);