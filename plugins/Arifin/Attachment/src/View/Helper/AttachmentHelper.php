<?php
namespace Arifin\Attachment\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;

class AttachmentHelper extends Helper
{
      
    public $helpers=['Form', 'Html', 'Url', 'General'];

    protected $_defaultConfig = [
        'label' => 'Attachments',
        // 'style' => [
        //     'background-image'=>'url("{{image.file}}")', 
        //     'background-size'=>'contain', 
        //     'width'=>'200px',
        //     'height'=>'200px', 
        //     'border'=>'1px solid #eee',
        //     'background-repeat'=> 'no-repeat', 
        //     'background-position'=> 'center',
        //     'margin' => '5px',
        //     'align' => 'center'
        // ]
        'style' => [
            'width'=>'200px',
            'height'=>'200px', 
            'border'=>'1px solid #eee',
            'margin' => '5px 5px 25px',
            'float'=> 'left',
            'text-align'=> 'center'
        ],
        'inner_style'=>[
            'background-image'=>'url("{{image.file}}")', 
            'background-size'=>'contain', 
            'background-repeat'=> 'no-repeat', 
            'background-position'=> 'center',
            'width'=>'100%',
            'height'=>'100%',
        ]
    ];

    public function getRoot(){
        // pr('Attachment.uploadPath');
        // pr(configure::read('UploadPath'));die;
        return configure::read('Root');
    }

    public function uploadPath(){
        // pr('Attachment.uploadPath');
        // pr(configure::read('UploadPath'));die;
        return configure::read('UploadPath');
    }


    public function control($label=null){

    	if (empty($label))
    		$label=$this->_config['label'];

        return $this->Form->control('attachments[]', ['type'=>'file','multiple','label'=> $label]);

    }

    public function controlUsingAngular($label, $data, $imageStyle=null){

    	if ($label!==null){
    		echo '<h3>'.$label.'</h3>';
    	}

        // pr($data);die;

        $attachments= $data['attachments'];
        $table= $data['table'];
        $id= $data['id'];

    	echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js');
        echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js');
	    echo $this->Html->script('Arifin/Attachment.ng-file-upload.min');
	    echo $this->Html->script('Arifin/Attachment.ng-file-upload-shim.min');
        echo $this->Html->script('Arifin/Attachment.ng-drag');
        // echo $this->Html->script('angular-file-upload/upload.js');        
	    echo $this->Html->script('Arifin/Attachment.upload');

        $root=$this->getRoot();
        // debug($this->fieldset);

        //make array of image source that will be assigned to ng-init,
        //then it will be passed to ng-repeat
        
        $attachments=array_map(function ($attachment) use ($root){
            // return $root.$attachment['filepath'].'/thumbs/'.$attachment['filename'];
            return [
                    'id'=> $attachment['id'],
                    'file'=> $root.$attachment['filepath'].'/thumbs/'.$attachment['filename']
                ];
        }
        , $attachments);

        $attachments=json_encode($attachments);

        //$style is used on div to contain image
        $style=$this->_config['style'];
        $inner_style=$this->_config['inner_style'];
      

        if (!empty($imageStyle)){

            $style= array_merge($style, $imageStyle);
        }

        $style= json_encode($style);
        $inner_style= json_encode($inner_style);



        echo '<div ng-app="fileUpload" ng-controller="MyCtrl">';

	        echo '<!-- init -->';
	        
	        echo '<div ng-init=\'
	                    images='.$attachments.';\'                                              
	              ></div>';

	        echo '<!-- list of images -->';
	        echo '

                <!--div class="columns" ng-repeat="image in images" style="width: 200px;height: 200px; margin: 5px; float:left"-->       
                <button class="button ng-hide" ng-click="saveNewSortOrder()" ng-show="buttonSaveNewSortOrderShow">save new sort order</button>
                <div class="thumbnail-container">
                    <div ng-repeat="image in images" ng-style=\''.$style.'\'>
                    <div class="thumbnail" draggable="true" data-id="{{image.id}}" ng-style=\''.$inner_style.'\' ng-dragstart="dragStart($event)" ng-dragleave="dragLeave($event)" ng-dragover="dragOver($event)" ng-drop="drop($event)">
                    </div>       
                    <br/>
                    <a href="javascript:void" ng-click="deleteImage($index+1,image.id)">delete</a>
                    </div>

                </div>

	         	<!--div class="" style="background-image:url({{image.file}}); background-size:contain; width:200px; height:200px; border:1px solid #eee; background-repeat: no-repeat; background-position: center">
	            </div-->

                <!--div ng-style=\''.$style.'\'></div-->

	        ';
	    // echo '</div>';

          

            echo ' <style type="text/css">
                .drop-box {
                    background: #F8F8F8;
                    border: 5px dashed #DDD;
                    /*width: 90%;*/
                    height: 150px;
                    text-align: center;
                    padding-top: 25px;
                    margin: 10px;
                }
                .dragover {
                    border: 5px dashed blue;
                }
                .progress-container{
                    display: block;
                    margin-bottom: 5px;
                }
                .progress-name{
                    width: 200px; 
                    float: left; 
                    
                }
                .progress {
                    display: inline-block;
                    width: 400px;
                    border: 1px groove #ddd;
                    margin: 0px;
                    padding: 0px;
                    height: 100%
                    
                }
                .ng-binding{
                    background-color: #c0c0c0;
                }
                .thumbnail{
                    float: left;
                }
            </style>';            

            // echo "name:".$this->name;

            $uploadUrl=$this->Url->build([
                "prefix" => false,
                "controller" => 'Arifin/Attachment',
                "action" => "upload",
                $id        
            ]);
            $saveNewSortOrderUrl=$this->Url->build([
                // "controller" => $this->name,
                "prefix" => false,
                "controller" => 'Arifin/Attachment',
                "action" => "saveSortOrder"            
            ]);
            $deleteAttachmentUrl=$this->Url->build([
                // "controller" => $this->name,
                "prefix" => false,
                "controller" => 'Arifin/Attachment',
                "action" => "delete"        
            ]);
            echo '<!-- init -->';
            echo '<div ng-init=\'
                id='.$id.';        
                table="'.$table.'";
                imagePath="'.$root.$this->uploadPath().'/";                
                uploadUrl="'.$uploadUrl.'";
                saveNewSortOrderUrl="'.$saveNewSortOrderUrl.'";
                deleteAttachmentUrl="'.$deleteAttachmentUrl.'"
                \'
            ></div>';

            echo '<div class="clearfix"></div>';     

            echo '<div>
                        <div id="drop-box" ngf-drop ngf-select ng-model="files" class="drop-box" 
                        ngf-drag-over-class="\'dragover\'" ngf-multiple="true" ngf-allow-dir="true"
                        accept="image/*" 
                        ngf-pattern="\'image/*\'"
            
                        >Drop images here or click to upload</div>
            
                        <div ngf-no-file-drop>File Drag/Drop is not supported for this browser</div>
            </div>';



            echo '<ul style="list-style: none;">
                <li ng-repeat="f in files">
                        
                  <div class="progress-container ng-hide" ng-show="f.progress < 100" >
                    <div class="progress-name" style="background-color: #fff;">{{f.name}}</div>  
                               
                    <div class="progress">
                        <div style="width:{{f.progress}}%;" ng-bind="f.progress + \'%\';" class="ng-binding">

                        </div>
                    </div>            
                  </div>

                </li>
            </ul>';

            // echo 'test:';
            // echo '<div>{{$id}}, {{$imagePath}}, {{$uploadPath}}, {{$attachments}} </div> ';   
            // echo '<div><pre>'.var_dump($this).'</pre></div> ';   

        echo '</div>';

	    echo '
        <script>
            
        $(document).ready(function(){

            function changeAttachmentSortId(draggingElem, dropElem, isInsertAfter, dragId, dropId){
                    // showBusy();
                    var formData = new FormData();
                    formData.append("dragId", dragId);          
                    formData.append("dropId", dropId);          
                    formData.append("isInsertAfter", isInsertAfter);            
                    formData.append("table", "'.$table.'");

                    $.ajax({
                      
                      cache: false,
                      // async: false,
                      processData: false,
                      contentType: false,
                      type: "POST",                      
                      url: "'.$this->Url->build([
                                    // "controller" => $this->name,
                                    "prefix" => false,
                                    "controller" => 'Arifin/Attachment',
                                    "action" => "reorder"                            
                                ]).'",
                      data: formData,
                      //}),
                      success: function(msg){             
                        // hideBusy();
                        console.log("message",msg);
                        var jsonObj=JSON.parse(msg);
                        var msg1;
                        if (jsonObj.result!=="ok"){
                          
                              msg1=(jsonObj.message);  
                              showMessage(msg1);            

                        }else{                
                            if (isInsertAfter){
                                draggingElem.insertAfter(dropElem);
                            }else{
                                draggingElem.insertBefore(dropElem);
                            }
                        }
                       
                      },
                      error: function(e) {
                        // hideBusy();
                        console.log("error",e);
                      }
                    });

                    
                }

            // var dragOverItem=null;
            // var thumbnailBorder=null;

            // $(".thumbnail-container")
            // .on("dragstart", ".thumbnail", function(e){
            //     // e.dataTransfer.setData("id", $(this).data("thumbnail"));
            //     data_id=$(this).data("id");

            //     thumbnailBorder=$(this).css("border");
                
            //     console.log("data id",data_id);
            //     console.log("e",e);
            // })
            // .on("dragleave",".thumbnail",function(e){
            //     console.log(">dragleave");
            //     // console.log($(this).data(id));
            //     // e.preventDefault();
            //     // if (dragOverItem==$(this).data("thumbnail")){
            //     // $(this).css("border","1px solid #ddd");
            //     // console.log("thumbnailBorder",thumbnailBorder);
            //     // $(this).css("border", thumbnailBorder);
            //     dragOverItem=null;
            //     // }
            // })
            // .on("dragover", ".thumbnail", function(e) {
                
            //     if ($(this).data("id")!==dragOverItem){

            //         console.log("data id",$(this).data("id"));

            //         $(".thumbnail").css("border", thumbnailBorder);
                                
            //         $(this).css("border","1px solid #0af");
            //         dragOverItem=$(this).data("id");

            //         console.log("dragOverItem", dragOverItem);
            //     }
            //     e.preventDefault();
            // })
            // .on("drop", ".thumbnail", function(e) {
            //     console.log("> on drop");
            //     // var id = e.dataTransfer.getData("thumbnail");
            //     // $(this).css("border","1px solid #ddd");
            //     $(this).css("border", thumbnailBorder);

            //     dragOverItem=null;

            //     var drag_id=data_id;
            //     var drop_id=$(this).data("id");

            //     console.log("drag_id", drag_id);
            //     console.log("drop_id", drop_id);
                
            //     var draggingElem = $(".thumbnail").filter(function() {
            //         return $(this).data("id") === drag_id;
            //     });
                
            //     var indexDrag = draggingElem.index();
            //     var indexThis = $(this).index();
            //     if(indexDrag < indexThis) {
            //         // draggingElem.insertAfter(this);
            //         console.log("indexDrag < indexThis");
            //         changeAttachmentSortId(draggingElem, this ,true, drag_id, drop_id);
            //     } else if(indexDrag > indexThis) {
            //         // draggingElem.insertBefore(this);
            //         console.log("indexDrag > indexThis");
                    
            //         changeAttachmentSortId(draggingElem, this, false, drag_id, drop_id);
            //     }
            // });


        });

        </script>';
    }
}
?>