<html>
<head>
<title>Transform Image with Cloudinary</title>

<style type="text/css">
	img{
		width: 200px;
		height: 150px;
	}
	li{
		margin: 10px 0;
	}
</style>

</head>
<body>
	 <h1>Upload to Cloudinary</h1>	
	<ul>
	<li>
	 <img src="img/world.jpg"/><br/>
	 <a href="transformimage.php?img=img/world.jpg">transform "world.jpg"</a>
	</li>
	<li>
	 <img src="img/raindrop.jpg"/><br/>
	 <a href="transformimage.php?img=img/raindrop.jpg">transform "raindrop.jpg"</a>
	</li>
	</ul>
</body>
</html