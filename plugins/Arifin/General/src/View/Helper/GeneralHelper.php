<?php
namespace Arifin\General\View\Helper;

use Cake\View\Helper;

class GeneralHelper extends Helper
{
    
    protected $_defaultConfig = [
        'rootList'=>[]
    ];    

    public function initialize(array $config){
        parent::initialize($config);        

        // $this->_defaultConfig=array_merge($this->_defaultConfig, $config);

        // debug($this->_defaultConfig);
        foreach ($config as $configkey => $configitem) {
            // debug ($configkey);  
            // debug ($configitem); 

            /*put / on last string*/
            if ($configkey=='rootList')
                foreach ($configitem as $rootListKey => $rootListItem) {
                    $a=substr($rootListItem,count($rootListItem)-2,1);
                    // debug($a);
                    if (substr($rootListItem,count($rootListItem)-2,1)!='/'){
                        $this->_config[$configkey][$rootListKey]=$rootListItem.'/';                        
                    }
                }        
        }

        // debug($this->_config);
    }

    function getRootList(){
        return $this->_config['rootList'];
    }

    public function prd($var){
    	return pr($var);
    	// die;
    }

    function getRoot(){
        $httpHost=$_SERVER['HTTP_HOST'];

        // if ($httpHost==='127.0.0.1'){
        //     return '/cake3test/';
        // }else
        // if ($httpHost==='test.server32.kmsoft.com.mk')
        // {
        //     return '/cake3/';
        // }

        foreach ($this->_config['rootList'] as $host => $root) {
            if ($httpHost==$host){
                return $root;
            }
        }

        return '/';
    }
}
?>