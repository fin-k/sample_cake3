<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PhotoLogs Controller
 *
 * @property \App\Model\Table\PhotoLogsTable $PhotoLogs
 *
 * @method \App\Model\Entity\PhotoLog[] paginate($object = null, array $settings = [])
 */
class PhotoLogsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $photoLogs = $this->paginate($this->PhotoLogs);

        $this->set(compact('photoLogs'));
        $this->set('_serialize', ['photoLogs']);
    }

    /**
     * View method
     *
     * @param string|null $id Photo Log id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $photoLog = $this->PhotoLogs->get($id, [
            'contain' => []
        ]);

        $photoLog= $this->Attachment->getAttachmentsOnResult($photoLog);        

        $this->set('photoLog', $photoLog);
        $this->set('_serialize', ['photoLog']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $photoLog = $this->PhotoLogs->newEntity();
        if ($this->request->is('post')) {
            $photoLog = $this->PhotoLogs->patchEntity($photoLog, $this->request->getData());
            if ($this->PhotoLogs->save($photoLog)) {
                $this->Flash->success(__('The photo log has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The photo log could not be saved. Please, try again.'));
        }
        $this->set(compact('photoLog'));
        $this->set('_serialize', ['photoLog']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Photo Log id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $photoLog = $this->PhotoLogs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $photoLog = $this->PhotoLogs->patchEntity($photoLog, $this->request->getData());
            if ($this->PhotoLogs->save($photoLog)) {
                $this->Flash->success(__('The photo log has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The photo log could not be saved. Please, try again.'));
        }
        $this->set(compact('photoLog'));
        $this->set('_serialize', ['photoLog']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Photo Log id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $photoLog = $this->PhotoLogs->get($id);
        if ($this->PhotoLogs->delete($photoLog)) {
            $this->Flash->success(__('The photo log has been deleted.'));
        } else {
            $this->Flash->error(__('The photo log could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
