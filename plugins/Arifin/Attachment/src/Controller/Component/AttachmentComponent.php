<?php
namespace Arifin\Attachment\Controller\Component;


use Cake\Controller\Component;
use Cake\Controller\Component\FlashComponent;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;


class AttachmentComponent extends Component
{	

	public $components = ['Flash'];

	protected $_defaultConfig = [
        'uploadPath' => 'files/attachments',
        'thumbPath' => 'files/attachments/thumbs',
        'thumbWidth' => 300
    ];

	public function initialize(array $config){
		parent::initialize($config);
		// debug($config);
		
		foreach ($config as $configkey => $configitem) {
			// debug ($configkey);	
			// debug ($configitem);	

			if ($configkey===('uploadPath')){
				$this->_config['thumbPath']= 
					$this->General->includeLastDelimiter($configitem).
					'thumb';

				// debug($this->_config['thumbsPath']);
			}
		}

		// if (!$this->checkAttachmentsTable()){
		// 	$this->createAttachmentsTable();
		// }
	}

	public function myGetConfig(){
		return $this->_config;
	}

    public function getTable(){
        $tablename=$this->_registry->getController()->loadModel(
                    $this->_registry->getController()->modelClass
                )-> getTable();
        return $tablename;
    }


    public function uploadAttachments($requestData, $foreign_key){

    	// $result= false;

    	// $model=$this->_registry->getController()->modelClass;
    	$model=$this->_registry->getController()->loadModel(
    				$this->_registry->getController()->modelClass
    			)-> getTable();

    	// pr($attachments);
    	// pr($model);
    	// pr($foreign_key);
    	// die;

    	$attachments= $requestData['attachments'];

    	try {
    		
			if (!empty($attachments)){

		        foreach ($attachments as $attachment ) {
		        	if (empty($attachment)) 
		        		continue;

		            $filename= $attachment['name'];
		            if (empty($filename)) continue;

		            $pos=strpos(WWW_ROOT, 'webroot');
		            // pr($pos);
		            
		            $this->createFolderRecursive($this->_config['uploadPath']);
		            $this->createFolderRecursive($this->_config['thumbPath']);

		            $uploadPath = substr(WWW_ROOT,0,$pos).$this->_config['uploadPath'];
		            $thumbPath = substr(WWW_ROOT,0,$pos).$this->_config['thumbPath'];
		            

		            $uploadFile = $this->includeLastDelimiter($uploadPath).$filename;

		            // pr($uploadFile);die;

		            if(move_uploaded_file($attachment['tmp_name'], $uploadFile)) {

		            	$this->make_thumb($uploadFile, $thumbPath.'/'.$filename, $this->_config['thumbWidth']);
		              	            
		                $connection = ConnectionManager::get('default');
		                $sql='insert into attachments(filepath, filename, filetype, filesize, model, foreign_key) values ("'.$this->_config['uploadPath'].'", "'.$attachment["name"].'", "'.$attachment["type"].'", '.$attachment["size"].', "'.$model.'", '.$foreign_key.');';

		                // pr($sql);die;
		                // try {
		                	$execresult= $connection->execute($sql);	
		                // } catch (Exception $e) {
		                	// pr($execresult->errorCode());
		                	// $this->Flash->error(__($execresult->errorCode()));
		                // }
		                		               

		                if (!$execresult){
		                	$this->Flash->error(__('Unable to update attachments table.'));
		                }
		                
		            }
		            else{
		                $this->Flash->error(__('Unable to upload file, please try again.'));
		                // return false;
		            }
		        }
		    }
		    // return true;

	    } catch (Exception $e) {
    		pr($e);
    	}

    	// return $result;
    }

    public function uploadAttachmentsUsingAngular($requestData, $foreign_key){

        //using angular, attachment is single file
        //uploading multiple file will be done separately in paralel
        $attachment= $requestData['attachment'];

        // pr($attachment);

        try {
            
            if (!empty($attachment) && !empty($attachment['name'])){

               // foreach ($attachments as $attachment ) {                                        

                    $hashId=substr(
                        sha1(
                            '-1#(*1jfanav;aSFDWEsl]'.$requestData['id'].date('dmyHisu')
                        )
                    ,0,12);                
                    $filename= $hashId.'_'.$attachment['name'];    


                    $pos=strpos(WWW_ROOT, 'webroot')-1;
                    // pr('WWW_ROOT: ');
                    // pr(WWW_ROOT);
                    
                
                    $uploadPath = substr(WWW_ROOT,0,$pos)."/webroot/".$this->_config['uploadPath'];
                    $thumbPath = substr(WWW_ROOT,0,$pos)."/webroot/".$this->_config['thumbPath'];
                    
                    $uploadFile = $this->includeLastDelimiter($uploadPath).$filename;

                    // pr($uploadFile);

                    $this->createFolderRecursive($this->_config['uploadPath']);
                    $this->createFolderRecursive($this->_config['thumbPath']);
                    
                    // die;

                    if(move_uploaded_file($attachment['tmp_name'], $uploadFile)) {

                        $this->make_thumb($uploadFile, $thumbPath.'/'.$filename, $this->_config['thumbWidth']);
                                    
                        $connection = ConnectionManager::get('default');
                        $sql='
                        SELECT ifnull(max(sort_id)+1,1) into @newSortId from attachments where model="'.$requestData["table"].'"
                            and foreign_key='.$foreign_key.'
                            ;
                        insert into attachments(filepath, filename, filetype, filesize, model, foreign_key, sort_id) values ("'.$this->_config['uploadPath'].'", "'.$filename.'", "'.$attachment["type"].'", '.$attachment["size"].', "'.$requestData["table"].'", '.$foreign_key.', @newSortId);';
                        

                        // pr($sql);die;
                        // try {
                        $execresult= $connection->execute($sql);

                        // $result= $execresult->fetchAll();
                        // } catch (Exception $e) {
                            // pr($execresult->errorCode());
                            // $this->Flash->error(__($execresult->errorCode()));
                        // }
                                               

                        if (!$execresult){
                            $this->Flash->error(__('Unable to update attachments table.'));
                        }else{
                            $execresult->closeCursor();

                            $id=$connection->query('select last_insert_id();')->fetch()[0];

                            // pr($execresult);
                            // pr($id);die;
                        }
                        
                    }
                    else{
                        $this->Flash->error(__('Unable to upload file, please try again.'));
                        // return false;
                    }
                //}
            }
            return ['id'=>$id, 'filename'=> $filename];

        } catch (Exception $e) {
            pr($e);
        }

        // return $result;
        // die;
    }


    public function getAttachments($foreign_key){
    	// $model= strtolower($this->_registry->getController()->modelClass);
    	// pr($this->_registry->getController()->modelClass);

    	$model=$this->_registry->getController()->loadModel(
    				$this->_registry->getController()->modelClass
    			)-> getTable();

 

    	$connection = ConnectionManager::get('default');     
        $sql='SELECT attachments.* FROM attachments
            left join '.$model.
            ' on attachments.foreign_key='.$model.'.id 
            where attachments.model="'.$model.'" and attachments.foreign_key='.$foreign_key.
            ' order by sort_id';
        // pr('sql:'.$sql);die;
        $attachments = $connection->execute($sql)->fetchAll('assoc');

        return $attachments;
    }

    public function getAttachmentsOnResult($data){
    	//get attachments nested on $data.
    	//Data can be Cake\\ORM\\ResultSet, array, or single object (got by using Model->get())

    	$table=$this->_registry->getController()->loadModel(
    				$this->_registry->getController()->modelClass
    			)-> getTable();

    	$connection = ConnectionManager::get('default');     

    	//convert to array if its ResultSet
        if (!is_array($data) && get_class($data)==="Cake\\ORM\\ResultSet")
        		$data=$data->toArray();

        
        if (is_array($data)){
        	//result is array
            // pr($data);
        	foreach ($data as $key => $value) {
                // pr('key='.$key);
                $id=$value["id"];
                // pr($id);
        		$sql='SELECT attachments.* FROM attachments
	            left join '.$table.
	            ' on attachments.foreign_key='.$table.'.id 
	            where attachments.model="'.$table.'" and attachments.foreign_key='.$id.
                ' order by sort_id';

                // pr($sql);
	            $attachments = $connection->execute($sql)->fetchAll('assoc');

	            $data[$key]->attachments= $attachments;
                $data[$key]->table = $table;
        	}

        }else{        	
        	//result is single object
        	
        	$sql='SELECT attachments.* FROM attachments
            left join '.$table.
            ' on attachments.foreign_key='.$table.'.id 
            where attachments.model="'.$table.'" and attachments.foreign_key='.$data['id'].
            ' order by sort_id';

            $attachments = $connection->execute($sql)->fetchAll('assoc');
        
            $data->attachments= $attachments;
            $data['table']= $table;
        }
        // pr($data);
        // die;
        return $data;
    }

    /**
    * function to handle reorder image request by drag drop
    */
    public function reorderAttachments($table, $isInsertAfter, $dragId, $dropId){

        // $table=$this->_registry->getController()->loadModel(
        //             $this->_registry->getController()->modelClass
        //         )-> getTable();

        // pr($this);
        // $table="test";
        // return ($table);

        $connection = ConnectionManager::get('default');     


        if ($isInsertAfter=='true'){
            $sql_update=
                "
                update attachments
                set sort_id=sort_id-1
                where 
                model='".$table."' and
                sort_id>@drag_sort_id and sort_id<=@drop_sort_id;\n
                ";
        }else{
            $sql_update=
                "
                update attachments
                set sort_id=sort_id+1
                where 
                model='".$table."' and
                sort_id>=@drop_sort_id and sort_id<@drag_sort_id;\n
                ";
        }

        $sql=
            "
            SELECT sort_id into @drag_sort_id
            from attachments
            where model='".$table."' and
            id='".$dragId."';\n

            select sort_id into @drop_sort_id
            from attachments        
            where model='".$table."' and
            id='".$dropId."';\n

            ".$sql_update." 

            update attachments
            set sort_id=@drop_sort_id
            where
            model='".$table."' and
            id='".$dragId."';\n
            ";
        // return $sql;
        // pr('sql: '+$sql);die;
        
        $execResult=$connection->execute($sql);
        
        return $execResult;
        
    }

    /**
    * function to handle save images' order request by button click
    */
    public function saveSortOrder($table, $images){
        // pr($foreign_key);
        // pr($table);
        $images=json_decode($images);
        // pr($images);

        $sql="";
        for ($i=0; $i < count($images); $i++) { 
            // pr($images[$i]->id);
            $newSortId=$i+1;
            $sql.='update attachments set sort_id='.$newSortId.' where model="'.$table.'" and id='.$images[$i]->id.';';
        }

        // pr($sql);

        $connection = ConnectionManager::get('default');  
        $execResult=$connection->execute($sql);
        
        return $execResult;   

    }


    public function delete($id){
        $connection = ConnectionManager::get('default');
        $sql='delete from attachments where id='.$id.';';  
        $execResult=$connection->execute($sql);

        return $execResult;
    }

    private function make_thumb($src, $dest, $desired_width) {



        $imageType=exif_imagetype ($src); $imageNotSupported=false;

		/* read the source image */
        switch ($imageType) {
            case IMAGETYPE_JPEG:
                $source_image = imagecreatefromjpeg($src);
                break;
            case IMAGETYPE_PNG:
                $source_image = imagecreatefrompng($src);
                break;
            case IMAGETYPE_GIF:
                $source_image = imagecreatefromgif($src);
                break;            
            default:                
                $imageNotSupported=true;
                break;
        }

        if (!$imageNotSupported){

            $width = imagesx($source_image);
            $height = imagesy($source_image);

            /**
            * resize if width>desired_width, copy otherwise.
            */
            if ($width>$desired_width){
                        
                /* find the "desired height" of this thumbnail, relative to the desired width  */
                $desired_height = floor($height * ($desired_width / $width));
                
                /* create a new, "virtual" image */
                $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
                
                /* copy source image at a resized size */
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
                /* create the physical thumbnail image to its destination */
                switch ($imageType) {
                    case IMAGETYPE_JPEG:
                        imagejpeg($virtual_image, $dest);
                        break;
                    case IMAGETYPE_PNG:
                        imagepng($virtual_image, $dest);
                        break;
                    case IMAGETYPE_GIF:
                        imagegif($virtual_image, $dest);
                        break;
                    default:
                        break;
                }
            }else{
                copy($src, $dest);
            }
        }
		
		
		
	}

	public function checkAttachmentsTable(){
		$connection = ConnectionManager::get('default');     
        $sql='SHOW tables like "%attachments%";';
        // pr('sql:'.$sql);
        $tables = $connection->execute($sql)->fetchAll('assoc');	

         // $sql='SELECT attachments.* FROM attachments
         //    left join products
         //     on attachments.foreign_key=products.id 
         //    where attachments.model="products" and attachments.foreign_key=1';
        // pr('sql:'.$sql);
        // $tables = $connection->execute($sql)->fetchAll('assoc');	

        foreach ($tables as $table) {
        	foreach ($table as $key => $value) {        		
        		// pr($key);	
        		// pr($value);	
        		if ($value==='attachments')
        			return true;
        		else return false;
        	}
        	
        }
        die;
	}

	//this function is not used, so table wont be created automatically
	public function createAttachmentsTable(){
		$connection = ConnectionManager::get('default');     
        $sql='
        CREATE TABLE `attachments` (
			  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
			  `filepath` varchar(255) NOT NULL,
			  `filename` varchar(255) NOT NULL,
			  `filetype` varchar(45) NOT NULL,
			  `filesize` int(10) NOT NULL,
			  `model` varchar(255) NOT NULL,
			  `foreign_key` char(36) NOT NULL,
			  `tags` text,
			  `created` datetime DEFAULT NULL,
			  `modified` datetime DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) DEFAULT CHARSET=utf8;
        ;';
        
        $connection->execute($sql);

	}
	
	private function includeLastDelimiter($path){
        $alen=strlen($path)-1;

        // pr($path);
        // pr($alen);
        // pr($path[$alen]);
        // die;

        // $backslash='\\';
        // pr($backslash);

        if (($path[$alen]!='/') && ($path[$alen]!="\\"))
        {
            return $path.'/';
        } else
            return $path;
    }

    private function createFolderRecursive($path){

        // $root= WWW_ROOT;

        // pr('path:');pr($path);

        // $pos= strpos($path,'webroot');
        // $path = substr($path,$pos+strlen('webroot')+1);

        $path = preg_split("/[\\/\\\ ]/", $path,null, PREG_SPLIT_NO_EMPTY);
        // $path = preg_split('_[\\\\/]_', $path,null, PREG_SPLIT_NO_EMPTY);        

        // pr('$path[]');
        // pr($path);

        // return $path;

        $fullPath=WWW_ROOT;

        foreach ($path as $folder) {
            $fullPath= $fullPath.$folder.'/';
            // pr($folder);   
            // pr($fullPath);   

            if (!file_exists($fullPath)){
                // pr("makedir");   
                mkdir($fullPath);
            }
        }
        // die;
    }
}

?>