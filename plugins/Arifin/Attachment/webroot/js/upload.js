 var app = angular.module('fileUpload', ['ngFileUpload','ngDrag']);

    app.controller('MyCtrl', ['$scope', 'Upload', '$timeout', '$http', function($scope, Upload, $timeout, $http) {
        
        // $scope.id=0;

        $scope.$watch('files', function () {
            $scope.uploadPic($scope.files);
        });
        
        $scope.uploadPic = function(files, errFiles) {

          $scope.files = files;
          $scope.errFiles = errFiles;
          

          console.log('files', files);
          console.log('id', $scope.id);

          angular.forEach(files, function(file){ 
            
            file.upload = Upload.upload({
              // url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
              // url: '/cake3test/upload.php',
              url: $scope.uploadUrl,
              data: {id: $scope.id, table:$scope.table, attachment: file},
            });
  
            file.upload.then(function (response) {
              $timeout(function () {
                file.result = response.data;
                console.log('response');
                console.log('response.data',file.result);
                console.log('file name', file.name);
                // console.log('image path', $scope.imagePath+file.result.filename);
                
                $newImage={'id': file.result.id, 'file':$scope.imagePath+'thumbs/'+file.result.filename};
                console.log('newImage', $newImage);
                $scope.images.push($newImage);
                console.log('images', $scope.images);
                
              });
            }, function (response) {
                
                if (response.status > 0)
                  $scope.errorMsg = response.status + ': ' + response.data;
  
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
          });
        }

        $scope.saveNewSortOrder= function(){            

          $http({
                method : "POST",
                url : $scope.saveNewSortOrderUrl,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: 
                  'id='+$scope.id+
                  '&table='+$scope.table+
                  '&images='+JSON.stringify($scope.images)

            }).then(function mySuccess(response) {
              console.log(response.data);
              $scope.buttonSaveNewSortOrderShow=false;
            }, function myError(response) {
              console.log(response);
              $scope.savingStatusVisible=false;
                $scope.error = response.statusText;
            });
        }

        $scope.deleteImage= function(idx,data_id=null){
          // console.log('delete idx:'+idx);
          // return
          if (confirm("Delete image?")){
             $http({
                  method : "POST",
                  url : $scope.deleteAttachmentUrl+'/'+data_id,
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  // data: 'id='+data_id                
  
              }).then(function mySuccess(response) {
                console.log(response.data);
                // console.log(response.data.id);
                // $scope.images.splice(idx,1);
  
                for (var i = 0; i<$scope.images.length; i++) {
                  // console.log('for id:'+$scope.images[i].id);
                  if ($scope.images[i].id==response.data.id){
                    // console.log('images.splice'+i);
                    $scope.images.splice(i,1);
                    break;
                  }
                }
                // $scope.buttonSaveNewSortOrderShow=false;
              }, function myError(response) {
                console.log(response);
                $scope.savingStatusVisible=false;
                $scope.error = response.statusText;
              });
          }
        }

        $scope.dragStart = function (e) {
            // console.log('test');
            // $(e.currentTarget).css('border', '1px solid red');
            var id = $(e.currentTarget).data('id');
            // $('.thumbnail').css('border','1px solid blue');
            $scope.drag_cssBorder=$(e.currentTarget).css('border');
            $scope.drag_id=id;
            console.log(id); 
        };
        $scope.dragLeave = function (e) {
         
            $scope.dragOver_id=null;            
            e.preventDefault();
        }; 
        $scope.dragOver = function (e) {
            // console.log('test');
            // $(e.currentTarget).css('border', '1px solid red');

            var id = $(e.currentTarget).data('id');

            $('.thumbnail').css('border', $scope.drag_cssBorder);
            $(e.currentTarget).css('border', '1px solid blue');
            // $scope.drag_id=id;

            if ($scope.dragOver_id!==id){
              $scope.dragOver_id=id;
              console.log(id); 
            }
            e.preventDefault();
        };

        $scope.drop = function (e) {
            
            $('.thumbnail').css('border', $scope.drag_cssBorder);
            // $(e.currentTarget).css('border', $scope.drag_cssBorder);
            var id = $(e.currentTarget).data('id');
            
            $scope.drop_id=id;       

            if ($scope.drop_id==$scope.drag_id)
                return;

            var image_drag=null;
            var image_drag_index=null;
            var image_drop_index=null;
            for (var i = 0; i<$scope.images.length; i++) {
              // console.log('image',$scope.images[i]);
              if ($scope.images[i].id==$scope.drag_id){
                image_drag_index=i;
                
                break;
              }
            }
            
            image_drag=$scope.images[i];                
            $scope.images.splice(i,1);
            
            for (var i = 0; i<$scope.images.length; i++) {
              // console.log('image',$scope.images[i]);
              if ($scope.images[i].id==$scope.drop_id){
                // image_drag=$scope.images[i];
                if (image_drag!==null){
                  image_drop_index=i;
                }

                break;
              }
            }

            if (image_drag_index>image_drop_index)
              $scope.images.splice(image_drop_index,0,image_drag);
            else if (image_drag_index<=image_drop_index)
              $scope.images.splice(image_drop_index+1,0,image_drag);

            $scope.buttonSaveNewSortOrderShow=true;

            console.log(id); 
            // console.log('images', $scope.images);
        };
        
    }]);