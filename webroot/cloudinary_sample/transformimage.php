
<?php
include 'php/Cloudinary.php';
include 'php/Uploader.php';
if (file_exists('php/settings.php')) {
  include 'php/settings.php';
}

function do_uploads($imgfile, $filename){
  $default_upload_options = array("tags" => "App");
  // $public_id= $filename;

  $apos= strpos($filename, '.');
  $public_id= substr($filename, 0, $apos);

  return \Cloudinary\Uploader::upload($imgfile,
      array_merge($default_upload_options));
}
?>

<html>

<head>
	<script src="javascript/cloudinary-core-shrinkwrap.min.js" type="text/javascript"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js' type="text/javascript"></script>
	<style type="text/css">
		.control{
			margin:5px 10px;
			/*border:1px solid black;	*/
		}
		.float-left{
			float: left;
		}
		.clear{
			clear: both;
		}
		.image-container{
			/*text-align: center;*/
			border-bottom:3px solid #ddd; 
			text-align: center;

		}
		.image{
			background-size:contain; 
			width:100%; 
			border:0px solid #eee; 
			background-repeat: no-repeat; 
			background-position: center; 
			background-color:white; 
			margin: 5px;
		}
		.title{
			font-weight: bold;
		}
	</style>

</head>

<body ng-app="CloudinaryApp">



<?php	

	$imgfile=realpath($_GET['img']);//$_FILES['attachments']['tmp_name'][0];
	$filename=basename($imgfile);//$_FILES['attachments']['name'][0];

	// echo $imgfile.";".$filename."<br/>";

	//copy image to temporary
	$d = new DateTime('NOW');
	$ts= $d->format('Ymd_His');

	copy($imgfile,$imgfile.'.backup_'.$ts);

	if (file_exists($imgfile)){
		$result=do_uploads($imgfile, $filename);	
?>

<div ng-controller="MyCtrl">

	<div ng-init='
		originalImageFile=<?php echo "\"".str_replace("\\", "\\\\", $imgfile)."\"";?>;
		public_id=<?php echo "\"".$result["public_id"]."\"";?>;
		originalWidth=<?php echo "".$result["width"]."";?>;
		originalHeight=<?php echo "".$result["height"]."";?>;
		format=<?php echo "\"".$result["format"]."\"";?>;
		init();
	'></div>
	
 	<div class="image-container" style="">
 		<div>
			<a href='{{imageUrlLink}}' target="_blank" style="">
				<!-- <img id="image" ng-src="{{imageUrl}}" style="width: 300px; height: auto"> -->
				<div style="background-image:url({{imageUrl}});height:200px;" class="image">			
				</div>
				<!-- <div style="width: 100px">test</div> -->
			</a>		
			<!-- <div style="height:200px;position:relative;top: 50%" class="image" ng-show="error">
				{{error}}
			</div> -->
		</div>
		
	</div>
	<div style="height: 10px; margin: 5px; text-align: center">
		<!-- <a href='{{imageUrlLink}}' target="_blank">{{imageUrlLink}}</a>  -->
		<span style="color:red" ng-show="error">{{error}}</span>
		<button name="apply-button" ng-click="setImage()" ng-show="applyButtonVisible ">Apply</button>
		<button ng-click="saveImage()" ng-show="saveImageVisible">Save Image</button>
		<span ng-show="savingStatusVisible">Saving...</span>
		<!-- {{applyButtonVisible}},{{error}} -->
	</div>
	<!-- {{originalImageFile}} -->

	<!-- <button name="rotate-button" ng-click="rotate()">Rotate</button> -->
	<div>
		
	</div>
	<br/>

	<div class="title">Size & Crop</div>
	<div class="clear">
		<div class="control float-left">		
			
			<input type="checkbox" ng-model="aspectRatioEnabled" ng-change="aspectRatioEnabledChange()"/>
			<label for="">aspect ratio</label><br/>	
			<label for="">width</label>
			<input type="number" ng-model="aspectRatioWidth" ng-change="aspectRatioWidthChange()" min="0" max="{{originalWidth}}" />		
			<label for="">height</label>
			<input type="number" ng-model="aspectRatioHeight" ng-change="aspectRatioHeightChange()" min="0" max="{{originalHeight}}"/>
			
		</div>
		<div class="control float-left">		
			<label for="">zoom</label><br/>			
			<input type="range"  ng-model="zoom" ng-change="zoomChange()" min="0" max="3" step="0.1" />
			<input type="number" ng-model="zoom" ng-change="zoomChange()" min="0" max="3">
		</div>		
	</div>
	<div class="clear">
		<div class="control float-left">
			<input type="checkbox" ng-model="widthEnabled" ng-change="widthEnabledChange()"/>
			<label for="">width</label><br/>
			<input type="range"  ng-model="width" ng-change="widthChange()" min="0" max="{{originalWidth}}" />
			<input type="number" ng-model="width" ng-change="widthChange()" min="0" max="{{originalWidth}}">
		</div>
		<div class="control float-left">
			<input type="checkbox" ng-model="heightEnabled" ng-change="heightEnabledChange()"/>
			<label for="">height</label><br/>
			<input type="range"  ng-model="height" ng-change="heightChange()" min="0" max="{{originalHeight}}" />
			<input type="number" ng-model="height" ng-change="heightChange()" min="0" max="{{originalHeight}}">
		</div>
		<div class="control float-left">
			<label for="">mode</label><br/>
			<select ng-model="crop" ng-change="cropChange()">
			<option ng-repeat="option in cropOptions" value="{{option.name}}">{{option.title}}</option>
			</select>			
		</div>{{cropId}}
	</div>

	<div class="clear">
		<div class="control float-left" ng-show="cropOptionsShowGravity.indexOf(crop)>=0">
			<label for="">gravity</label><br/>
			<select ng-model="gravity" ng-change="gravityChange()">
			<option ng-repeat="option in gravityOptions" value="{{option}}">{{option}}</option>
			</select>			
		</div>
		
		<div class="control float-left" ng-show="crop=='crop'">		
			<label for="">x</label><br/>
			<input type="range"  ng-model="x" ng-change="xChange()" min="0" max="{{originalWidth}}" />
			<input type="number" ng-model="x" ng-change="xChange()" min="0" max="{{originalWidth}}">
		</div>
		<div class="control float-left" ng-show="crop=='crop'">		
			<label for="">y</label><br/>
			<input type="range"  ng-model="y" ng-change="yChange()" min="0" max="{{originalWidth}}" />
			<input type="number" ng-model="y" ng-change="yChange()" min="0" max="{{originalWidth}}">
		</div>

	</div>
	<div class="title clear">Format & Shape</div>
	<!-- <div class="clear">
		<div class="control float-left">
			<label for="">format</label><br/>
			<select ng-model="format"  ng-change="formatChange()">
			<option ng-repeat="option in formatOptions" value="{{option}}">{{option}}</option>
			</select>
		</div>
		<div class="control float-left">		
			<label for="">quality</label><br/>
			<input type="range"  ng-model="quality" ng-change="qualityChange()" min="0" max="100" />
			<input type="number" ng-model="quality" ng-change="qualityChange()" min="0" max="100">
		</div>
	</div> -->
	<div class="clear"> 
		<div class="control float-left">		
			<label for="">corner radius</label><br/>
			<input type="range"  ng-model="radius" ng-change="radiusChange()" min="0" max="100" />
			<input type="number" ng-model="radius" ng-change="radiusChange()" min="0" max="100">
		</div>
		<div class="control float-left">		
			<label for="">angle</label><br/>
			<input type="range"  ng-model="angle" ng-change="angleChange()" min="0" max="360" />
			<input type="number" ng-model="angle" ng-change="angleChange()" min="0" max="360">
		</div>
	</div>

	
	<div class="title clear">Effect</div>
	<div class="clear">
		<div class="control float-left">
			<label for="">effect</label><br/>
			<select ng-model="effect"  ng-change="effectChange()">
				<option value="" selected>none</option>
				<option ng-repeat="option in effectOptions" value="{{option.name}}">{{option.title}}</option>
			</select>
		</div>{{$effect}}
		<div class="control float-left" ng-show="effectData.type=='number'">		
			<label for=""></label><br/>			
			<input type="range"  ng-model="effectNumber" ng-change="effectNumberChange()" min="{{effectData.min}}" max="{{effectData.max}}" />
			<input type="number" ng-model="effectNumber" ng-change="effectNumberChange()" min="{{effectData.min}}" max="{{effectData.max}}">

		</div>
		<div class="control float-left" ng-show="effectData.type=='select'">		
			<label for=""></label><br/>
			<select ng-model="effectSelect"  ng-change="effectSelectChange()">
				<option value="" disabled selected hidden>choose filter...</option>
				<option ng-repeat="option in effectData.select" value="{{option}}">{{option}}</option>
			</select>
		</div>
	</div>
	<div class="title clear">Other</div>
	<div class="clear">
		<div class="control float-left">		
			<label for="">opacity</label><br/>			
			<input type="range"  ng-model="opacity" ng-change="opacityChange()" min="0" max="100" />
			<input type="number" ng-model="opacity" ng-change="opacityChange()" min="0" max="100"/>
		</div>
		<div class="control float-left">		
			<input type="checkbox" ng-model="borderEnabled" ng-change="borderEnabledChange()"/>
			<label for="">border</label><br/>
			<input type="range"  ng-model="border" ng-change="borderChange()" min="0" max="100" />
			<input type="number" ng-model="border" ng-change="borderChange()" min="0" max="100">
			<input type="color" ng-model="borderColor" ng-change="borderColorChange()"/>
		</div>
		<div class="control float-left">		
			<input type="checkbox" ng-model="backgroundEnabled" ng-change="backgroundEnabledChange()"/>	
			<label for="">background</label><br/>
			<input type="color" ng-model="backgroundColor" ng-change="backgroundColorChange()"/>		
			<input type="checkbox" ng-model="backgroundAuto" ng-change="backgroundAutoChange()"/>
			<label for="">auto</label>	
		</div>		
	</div>
	<div class="clear">
		
	<!-- 	<div class="control float-left">		
			<label for="">aspect ratio</label><br/>	
			<input type="checkbox" ng-model="aspectRatioEnabled" ng-change="aspectRatioEnabledChange()"/>		
			<input type="text" ng-model="aspectRatio" ng-change="aspectRatioChange()" min="0" max="100" size="4">
			
		</div> -->
		
	</div>
	
	<br/><br/>
	<!-- {{tr}} -->
	<!-- {{effectData}} -->
	<!-- <input type="edit" ng-model="test1" ng-change="testfunction()">{{test2}} -->

</div>

	
<?php
	
	}else{
		echo $imgfile." doesn't exist";
	}
?>
	
	<script src="javascript/transformimage.js"></script>

</body>

</html>