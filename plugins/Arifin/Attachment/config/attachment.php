<?php

return [
	'Root' => '/',
	'UploadPath' => 'files/attachments',	
    'ThumbPath' => 'files/attachments/thumbs',
    'ThumbWidth' => 300
];