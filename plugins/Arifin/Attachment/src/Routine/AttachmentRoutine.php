<?php
namespace Arifin\Attachment\Routine;


use Cake\Controller\Component;
use Cake\Controller\Component\FlashComponent;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;


// function testprint2($str){
// 		pr($str);
// }

class AttachmentRoutine
{	

	public static function testprint($str){
		pr($str);
	}

	public static function make_thumb($src, $dest, $desired_width) {

		/* read the source image */
		$source_image = imagecreatefromjpeg($src);
		$width = imagesx($source_image);
		$height = imagesy($source_image);
		
		/* find the "desired height" of this thumbnail, relative to the desired width  */
		$desired_height = floor($height * ($desired_width / $width));
		
		/* create a new, "virtual" image */
		$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
		
		/* copy source image at a resized size */
		imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
		
		/* create the physical thumbnail image to its destination */
		imagejpeg($virtual_image, $dest);
	}

	public static function includeLastDelimiter($path){
        $alen=strlen($path)-1;

        // pr($path);
        // pr($alen);
        // pr($path[$alen]);
        // die;

        // $backslash='\\';
        // pr($backslash);

        if (($path[$alen]!='/') && ($path[$alen]!="\\"))
        {
            return $path.'/';
        } else
            return $path;
    }

    public static function createFolderRecursive($path){

        // $root= WWW_ROOT;

        $pos= strpos($path,'webroot');
        $path = substr($path,$pos+strlen('webroot')+1);

        $path = preg_split('/[\\/\\\ ]/', $path,null, PREG_SPLIT_NO_EMPTY);

        // return $path;

        $fullPath=WWW_ROOT;

        foreach ($path as $folder) {
            $fullPath= $fullPath.$folder.'/';
            // pr($fullPath);   

            if (!file_exists($fullPath)){
                mkdir($fullPath);
            }
        }
        // die;
    }

	public static function uploadAttachments($attachments, $foreign_key, $model, $uploadPath){

    	// $result= false;

    	// $model=$this->_registry->getController()->modelClass;
    	// $model=$this->_registry->getController()->loadModel(
    	// 			$this->_registry->getController()->modelClass
    	// 		)-> getTable();

    	// pr($attachments);
    	// pr($model);
    	// pr($foreign_key);
    	// die;

    	// $attachments= $requestData['attachments'];

    	$this->testprint($model);
    	// die;

    	try {
    		
			if (!empty($attachments)){

		        foreach ($attachments as $attachment ) {
		        	if (empty($attachment)) 
		        		continue;

		            $filename= $attachment['name'];
		            if (empty($filename)) continue;

		            $pos=strpos(WWW_ROOT, 'webroot');
		            // pr($pos);

		            $thumbPath=$uploadPath.'/thumbs';		            
		            
		            createFolderRecursive($uploadPath);
		            createFolderRecursive($thumbPath);

		            $uploadPath = substr(WWW_ROOT,0,$pos).$uploadPath;
		            $thumbPath = substr(WWW_ROOT,0,$pos).$thumbPath;
		            

		            $uploadFile = includeLastDelimiter($uploadPath).$filename;

		            // pr($uploadFile);die;

		            if(move_uploaded_file($attachment['tmp_name'], $uploadFile)) {

		            	make_thumb($uploadFile, $thumbPath.'/'.$filename, 300);
		              	            
		                $connection = ConnectionManager::get('default');
		                $sql='insert into attachments(filepath, filename, filetype, filesize, model, foreign_key) values ("'.$this->_config['uploadPath'].'", "'.$attachment["name"].'", "'.$attachment["type"].'", '.$attachment["size"].', "'.$model.'", '.$foreign_key.');';

		                // pr($sql);die;
		                // try {
		                	$execresult= $connection->execute($sql);	
		                // } catch (Exception $e) {
		                	// pr($execresult->errorCode());
		                	// $this->Flash->error(__($execresult->errorCode()));
		                // }
		                		               

		                if (!$execresult){
		                	$this->Flash->error(__('Unable to update attachments table.'));
		                }
		                
		            }
		            else{
		                $this->Flash->error(__('Unable to upload file, please try again.'));
		                // return false;
		            }
		        }
		    }
		    // return true;

	    } catch (Exception $e) {
    		pr($e);
    	}

    	// return $result;
    }

   
}
?>