<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\Router;

Router::plugin(
    'Arifin/Attachment', ['path' => '/arifin/attachment'], function (RouteBuilder $routes) {
    	 // $routes->connect('/:controller');

    	$routes->connect('/', ['controller' => 'Attachment', 'action' => 'index']);
    	$routes->connect('/view', ['controller' => 'Attachment', 'action' => 'view']);
    	$routes->connect('/upload/*', ['controller' => 'Attachment', 'action' => 'upload']);
    	$routes->connect('/reorder', ['controller' => 'Attachment', 'action' => 'reorder']);
    	$routes->connect('/save-sort-order', ['controller' => 'Attachment', 'action' => 'saveSortOrder']);
    	$routes->connect('/delete/*', ['controller' => 'Attachment', 'action' => 'delete']);

        $routes->fallbacks(DashedRoute::class);
    }
);
