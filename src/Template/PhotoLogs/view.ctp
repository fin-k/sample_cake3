<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\PhotoLog $photoLog
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Photo Log'), ['action' => 'edit', $photoLog->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Photo Log'), ['action' => 'delete', $photoLog->id], ['confirm' => __('Are you sure you want to delete # {0}?', $photoLog->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Photo Logs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Photo Log'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="photoLogs view large-9 medium-8 columns content">
    <h3><?= h($photoLog->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($photoLog->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($photoLog->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($photoLog->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($photoLog->modified) ?></td>
        </tr>
    </table>
     <?php
        $imageStyle= [
            'border'=> '3px solid #1798A5',
            'background-color'=> '#eee',
            'border-radius' => '20px',
            'width' => '150px',
            'height' => '150px',
            'margin' => '15px 15px 25px'
        ];
            
        $this->Attachment->controlUsingAngular('Photos', $photoLog, $imageStyle);
    ?>
</div>
