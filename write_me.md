 WriteMe.md
### A simple, real-time Markdown editor with GitHub and Bitbucket themes

---

To use it, simply:

* Type Markdown text in the left pane
* See the HTML in the right

**WriteMe.md** also supports GitHub-style syntax highlighting for numerous languages, like so:

```html
<h1>Enjoy</h1>
```

```css
h1:after {
  content: 'using';
}
```

```js
console.log('WriteMe.md');
```


```php
$imageStyle= [
   'border'=> '3px solid #1798A5',
   'background-color'=> '#eee',
   'border-radius' => '20px',
   'width' => '150px',
   'height' => '150px',
   'margin' => '15px'
];
      
$this->Attachment->controlUsingAngular('Photos', $photoLog, $imageStyle);

```

---

To learn the basics of using Markdown, **[read this](http://daringfireball.net/projects/markdown/basics)**.