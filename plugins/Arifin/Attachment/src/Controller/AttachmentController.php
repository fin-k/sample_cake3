<?php

// plugins/ContactManager/src/Controller/AttachmentController.php
namespace Arifin\Attachment\Controller;

use Arifin\Attachment\Controller\AppController;

class AttachmentController extends AppController
{

    public function index()
    {
        pr('this is index');die;
    }

    public function view()
    {
    	pr('this is view');die;
    }


    public function upload($id)
    {
        // pr($this->request->getData());
        // pr($_POST);        
        // die;

        $result=$this->Attachment->uploadAttachmentsUsingAngular($this->request->getdata(), $id);

        echo json_encode($result);
        // echo '{"filename":"'.$result.'"}';

        die; //call die so it doesn't need view.
    }

    public function reorder()
    {
        // pr($this->request->getData());
        // pr($_POST);        
        
        $result=$this->Attachment->reorderAttachments($_POST['table'], $_POST['isInsertAfter'],$_POST['dragId'],$_POST['dropId']);
        
        // pr($result);

        // echo '{"result":"'.$result.'"}'; 
        // die;
        
        if (!$result){
            echo '{"result":"'.$result.'"}'; 
        }else{
            echo '{"result":"ok"}';
        }
        
        // echo '{"filename":'.$result.'"}';

        die; //call die so it doesn't need view.
    }

    public function saveSortOrder(){
        // pr('saveSortOrder function');
        // $data=json_decode($this->request->getData()['images']);
        // var_dump($data[0]);
        $result=$this->Attachment->saveSortOrder($_POST['table'],$_POST['images']);

        if (!$result){
            echo '{"result":"'.$result.'"}'; 
        }else{
            echo '{"result":"ok"}';
        }

        die;
    }

    public function delete($id){
        $result=$this->Attachment->delete($id);

        if (!$result){
            echo '{"result":"'.$result.'"}'; 
        }else{
            echo '{"result":"ok", "id": '.$id.'}';
        }

        // echo '{"result":"ok"}';

        die;
    }
}

?>