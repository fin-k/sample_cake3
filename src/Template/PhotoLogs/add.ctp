<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Photo Logs'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="photoLogs form large-9 medium-8 columns content">
    <?= $this->Form->create($photoLog) ?>
    <fieldset>
        <legend><?= __('Add Photo Log') ?></legend>
        <?php
            echo $this->Form->control('title');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
