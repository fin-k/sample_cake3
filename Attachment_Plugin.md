# Cake Attachment Plugin 

**Features** : 

* Upload using angularjs.
* Sorting image by drag and drop.

# How to Use

* Create table attachments.

```mysql
CREATE TABLE `attachments` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `filepath` varchar(255) NOT NULL,
    `filename` varchar(255) NOT NULL,
    `filetype` varchar(45) NOT NULL,
    `filesize` int(10) NOT NULL,
    `model` varchar(255) NOT NULL,
    `foreign_key` char(36) NOT NULL,
    `tags` text,
    `sort_id` int(10) unsigned DEFAULT NULL,
    `created` datetime DEFAULT NULL,
    `modified` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
```

* Copy plugin's folder "Arifin" to plugin directory.

* Load plugin in "/config/bootstrap.php".

```php
Plugin::load('Arifin/Attachment', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);
```

* Load attachment component.

"/src/Controller/AppController.php":

```php
public function initialize()
{
    parent::initialize();
	$this->loadComponent('Arifin/Attachment.Attachment'); 
}
```

* Load attachment helper on initialize().

"/src/View/AppView.php":

```php
  public function initialize()
  {
    parent::initialize();
    $this->loadHelper('Arifin/Attachment.Attachment');
  }
```

* Get attachment items. 

  To get attachment items call getAttachmentsOnResult function on controller's action, with query result or array result is passed as a parameter. For example codes below call the function on action view.

"/src/Controller/PhotoLogsController.php":

```php
 public function view($id = null)
 {
   $photoLog = $this->PhotoLogs->get($id, [
     'contain' => []
   ]);

   $photoLog= $this->Attachment->getAttachmentsOnResult($photoLog);              

   $this->set('photoLog', $photoLog);
   $this->set('_serialize', ['photoLog']);
 }
```

​	If we call ```pr($photoLog)``` we can see the attachment items is put on result as array.

```
App\Model\Entity\PhotoLog Object
(
    [id] => 1
    [title] => title1
    [table] => photo_logs
    [attachments] => Array
        (
            [0] => Array
                (
                    [id] => 46
                    [filepath] => files/attachments
                    [filename] => 9d906d47c9a1_pexels-photo.jpg
                    [filetype] => image/jpeg
                    [filesize] => 426444
                    [model] => photo_logs
                    [foreign_key] => 1
                    [tags] => 
                    [sort_id] => 1
                    [created] => 
                    [modified] => 
                )
         )
 )
```

* Put control on view.

  To show list of images and drop box to upload the file, call controlUsingAngular function on view. For example codes below call the function on view.ctp and using customized image style to stylize the image items.


"/src/Template/PhotoLogs/view.php":

```php
$imageStyle= [
   'border'=> '3px solid #1798A5',
   'background-color'=> '#eee',
   'border-radius' => '20px',
   'width' => '150px',
   'height' => '150px',
   'margin' => '15px'
];
      
$this->Attachment->controlUsingAngular('Photos', $photoLog, $imageStyle);
```

  If we want to use the default image style, call the function without $imageStyle parameter.

```php
$this->Attachment->controlUsingAngular('Photos', $photoLog);
```

  