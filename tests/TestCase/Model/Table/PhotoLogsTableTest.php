<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PhotoLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PhotoLogsTable Test Case
 */
class PhotoLogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PhotoLogsTable
     */
    public $PhotoLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.photo_logs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PhotoLogs') ? [] : ['className' => PhotoLogsTable::class];
        $this->PhotoLogs = TableRegistry::get('PhotoLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PhotoLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
