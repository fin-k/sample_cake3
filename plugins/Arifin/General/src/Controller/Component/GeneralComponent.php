<?php
namespace Arifin\General\Controller\Component;

use Cake\Controller\Component;

class GeneralComponent extends Component
{
	protected $_defaultConfig = [
        'rootList'=>[]
    ];    

    public function initialize(array $config){
        parent::initialize($config);        

        // $this->_defaultConfig=array_merge($this->_defaultConfig, $config);

        // debug($this->_defaultConfig);
        foreach ($config as $configkey => $configitem) {
            // debug ($configkey);  
            // debug ($configitem); 

            /*put / on last string*/
            if ($configkey=='rootList')
                foreach ($configitem as $rootListKey => $rootListItem) {
                    $a=substr($rootListItem,count($rootListItem)-2,1);
                    // debug($a);
                    if (substr($rootListItem,count($rootListItem)-2,1)!='/'){
                        $this->_config[$configkey][$rootListKey]=$rootListItem.'/';                        
                    }
                }        
        }

        // debug($this->_config);
    }

    function getRootList(){
        return $this->_config['rootList'];
    }

	function getRoot(){
		$httpHost=$_SERVER['HTTP_HOST'];

		// if ($httpHost==='127.0.0.1'){
		// 	return '/cake3test/';
		// }

        foreach ($this->_config['rootList'] as $host => $root) {
            if ($httpHost==$host){
                return $root;
            }
        }
        
		return '/';
	}

    public function prd($var)
    {
        pr($var);
        die;
        return;
    }

    public function getComponent(){
    	return $this->_registry->getController();
    }

    public function includeLastDelimiter($path){
        $alen=strlen($path)-1;

        // pr($path);
        // pr($alen);
        // pr($path[$alen]);
        // die;

        // $backslash='\\';
        // pr($backslash);

        if (($path[$alen]!='/') && ($path[$alen]!="\\"))
        {
            return $path.'/';
        } else
            return $path;
    }

    public function createFolderRecursive($path){

        // $root= WWW_ROOT;

        $pos= strpos($path,'webroot');
        $path = substr($path,$pos+strlen('webroot')+1);

        $path = preg_split('/[\\/\\\ ]/', $path,null, PREG_SPLIT_NO_EMPTY);

        // return $path;

        $fullPath=WWW_ROOT;

        foreach ($path as $folder) {
            $fullPath= $fullPath.$folder.'/';
            // pr($fullPath);   

            if (!file_exists($fullPath)){
                mkdir($fullPath);
            }
        }
        // die;
    }
}

?>